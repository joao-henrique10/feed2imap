exit if RUBY_VERSION < '1.9'

tests = Dir.glob('test/tc_*.rb') - ['test/tc_httpfetcher.rb']
tests.each { |t| require t }
